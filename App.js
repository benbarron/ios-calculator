import React, { Component } from 'react'
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures'

import { StyleSheet, View, Text, TextInput } from 'react-native'

import BasicButtons from './components/Buttons/BasicButtons'
import ComplexButtons from './components/Buttons/ComplexButtons'

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#222',
    flex: 1
  },
  buttonsWrapper: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',

    flex: 5,
    paddingBottom: 10
  },
  inputWrapper: {
    width: '100%',
    marginTop: 0,
    flex: 3,
    display: 'flex',
    justifyContent: 'flex-end'
  },
  input: {
    width: '100%',
    padding: 10,
    fontSize: 85,
    marginTop: 0,
    marginBottom: 10,
    textAlign: 'right',
    color: '#fff'
  },
  operationSign: {
    color: '#fff',
    marginTop: 50,
    width: '95%',
    textAlign: 'right'
  }
})

export default class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      input: '',
      memInput: '',
      tempInput: '0',
      operator: '',
      step: 0,
      buttonSet: 0
    }
  }

  onSwipe(gestureName, gestureState) {
    const { SWIPE_LEFT, SWIPE_RIGHT } = swipeDirections

    switch (gestureName) {
      case SWIPE_LEFT:
        this.setState({ buttonSet: 1 })
        break
      case SWIPE_RIGHT:
        this.setState({ buttonSet: 0 })
        break
      default:
        break
    }
  }

  backSpace() {
    var temp = Array.from(String(this.state.input))
    temp.splice(0, 1)

    var out = ''

    for (let i = 0; i < temp.length; i++) {
      out += temp[i]
    }

    this.setState({ input: String(out) })
  }

  appendInput(num) {
    var temp = Number(this.state.input)

    if (this.state.step == 0) {
      this.setState({ input: String(temp) + String(num) })
    } else {
      this.setState({ input: `${num}`, step: 0 })
    }
  }

  clearInput() {
    this.setState({ input: '', operator: '' })
  }

  changeInputSign() {
    if (!this.state.input) {
      return
    }

    var temp = Number(this.state.input)
    var newNumber = temp * -1
    this.setState({ input: `${newNumber}` })
  }

  changeToPercent() {
    if (this.state.input) {
      this.setState({ input: `${Number(this.state.input) / 100}` })
    }
  }

  setOperator(operation) {
    this.setState({ operator: operation, memInput: this.state.input, step: 1 })
  }

  complexEvaluation(func) {
    if (!this.state.input) {
      return
    }

    var rads = Number(this.state.input) * (Math.PI / 180)

    switch (func) {
      case 'sin':
        var answer = Math.sin(rads)
        break
      case 'cos':
        var answer = Math.cos(rads)
        break
      case 'tan':
        var answer = Math.tan(rads)
        break
      case 'sinh':
        var answer = Math.sinh(rads)
        break
      case 'cosh':
        var answer = Math.cosh(rads)
        break
      case 'tanh':
        var answer = Math.tanh(rads)
        break
      case 'exp':
        var answer = Math.exp(Number(this.state.input))
        break
      case 'naturalLog':
        var answer = Math.LN10(Number(this.state.input))
        break
      case 'log':
        var answer = Math.log10(Number(this.state.input))
        break
      case 'sqrt':
        var answer = Math.sqrt(Number(this.state.input))
        break
      case 'factorial':
        var num = Math.ceil(Number(this.state.input))
        var final = 1

        for (let i = 1; i <= num; i++) {
          var final = final * i
        }

        var answer = final
        break
      case 'inverse':
        var answer = 1 / Number(this.state.input)
        break
      case 'squared':
        var answer = Number(this.state.input) * Number(this.state.input)
        break

      case 'cubed':
        var answer =
          Number(this.state.input) *
          Number(this.state.input) *
          Number(this.state.input)
      default:
        break
    }

    this.setState({ input: String(answer), step: 2 })
  }

  randomNumber() {
    this.setState({ input: String(Math.random()) })
  }

  toDegrees() {
    var answer = Number(this.state.input) * (180 / Math.PI)

    this.setState({ input: String(answer), step: 2 })
  }

  toRadians() {
    var answer = Number(this.state.input) * (Math.PI / 180)

    this.setState({ input: String(answer), step: 2 })
  }

  evaluateInput() {
    this.setState({ operator: '' })

    if (this.state.step == 1) {
      return
    }

    switch (this.state.operator) {
      case 'add':
        var answer = Number(this.state.memInput) + Number(this.state.input)
        break
      case 'subtract':
        var answer = Number(this.state.memInput) - Number(this.state.input)
        break
      case 'multiply':
        var answer = Number(this.state.memInput) * Number(this.state.input)
        break
      case 'divide':
        var answer = Number(this.state.memInput) / Number(this.state.input)
        break
      case 'power':
        var number = Number(this.state.memInput)
        var power = Number(this.state.input)
        var final = 1

        for (let i = 1; i <= power; i++) {
          var final = final * number
        }

        var answer = final

        break
      default:
        break
    }

    this.setState({ input: String(answer), step: 2 })
  }

  render() {
    if (this.state.operator == 'divide') {
      var operationSign = '/'
    } else if (this.state.operator == 'multiply') {
      var operationSign = 'x'
    } else if (this.state.operator == 'subtract') {
      var operationSign = '-'
    } else if (this.state.operator == 'add') {
      var operationSign = '+'
    } else if (this.state.operator == 'power') {
      var operationSign = '^'
    } else {
      var operationSign = ' '
    }

    return (
      <View style={styles.container}>
        <GestureRecognizer
          style={styles.inputWrapper}
          onSwipe={(direction, state) => this.backSpace()}
        >
          <Text style={styles.operationSign}>{operationSign}</Text>

          <TextInput
            editable={false}
            style={styles.input}
            placeholder={'0'}
            value={this.state.input ? this.state.input : this.state.tempInput}
          />
        </GestureRecognizer>
        <GestureRecognizer
          onSwipe={(direction, state) => this.onSwipe(direction, state)}
          style={styles.buttonsWrapper}
        >
          {this.state.buttonSet == 0 ? (
            <BasicButtons
              setOperator={arg => this.setOperator(arg)}
              appendInput={arg => this.appendInput(arg)}
              evaluateInput={() => this.evaluateInput()}
              clearInput={() => this.clearInput()}
              changeInputSign={() => this.changeInputSign()}
              changeToPercent={() => this.changeToPercent()}
            />
          ) : null}

          {this.state.buttonSet == 1 ? (
            <ComplexButtons
              randomNumber={() => this.randomNumber()}
              toDegrees={arg => this.toDegrees(arg)}
              toRadians={arg => this.toRadians(arg)}
              complexEvaluation={arg => this.complexEvaluation(arg)}
              setOperator={arg => this.setOperator(arg)}
              appendInput={arg => this.appendInput(arg)}
              evaluateInput={() => this.evaluateInput()}
            />
          ) : null}
        </GestureRecognizer>
      </View>
    )
  }
}
