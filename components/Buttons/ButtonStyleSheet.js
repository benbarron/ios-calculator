import { StyleSheet, Dimensions } from 'react-native'

const screenWidth = Dimensions.get('window').width
const buttonSize = (screenWidth / 4) * 0.8

export default styles = StyleSheet.create({
  row: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    flex: 1,
    textAlign: 'center',
    justifyContent: 'flex-start'
  },
  button: {
    width: '25%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  touchable: {
    width: buttonSize,
    height: buttonSize,
    borderRadius: buttonSize,
    margin: 0,
    backgroundColor: '#333',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 10
  },
  buttonText: {
    textAlign: 'center',
    fontSize: 35,
    color: '#fff',
    textAlignVertical: 'center',
    textAlign: 'center'
  },
  buttonTextDark: {
    textAlign: 'center',
    fontSize: 35,
    color: '#111',
    textAlignVertical: 'center',
    textAlign: 'center'
  },
  buttonTextSmall: {
    marginTop: 10,
    fontSize: 18,
    color: '#fff',
    textAlign: 'center'
  },
  numberButton: {
    backgroundColor: '#4444'
  },
  bgBlue: {
    backgroundColor: '#0984e3'
  },
  bgDarkBlue: {
    backgroundColor: '#2980b9',
    borderColor: '#333'
  },
  bgLightGrey: {
    backgroundColor: '#666'
  },
  bgDarkGrey: {
    backgroundColor: '#444'
  }
})
