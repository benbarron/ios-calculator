import React, { Component } from 'react'

import { View, TouchableHighlight, Text, StyleSheet } from 'react-native'

import styles from './ButtonStyleSheet'

export default class BasicButtons extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View>
        <View style={styles.row}>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable, styles.bgLightGrey]}
              onPress={() => this.props.clearInput()}
            >
              <Text style={styles.buttonTextSmall}>AC</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable, styles.bgLightGrey]}
              onPress={() => this.props.changeInputSign()}
            >
              <Text style={styles.buttonTextSmall}>+/-</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable, styles.bgLightGrey]}
              onPress={() => this.props.changeToPercent()}
            >
              <Text style={styles.buttonTextSmall}>%</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable, styles.bgBlue]}
              onPress={() => this.props.setOperator('divide')}
            >
              <Text style={styles.buttonTextSmall}>/</Text>
            </TouchableHighlight>
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.appendInput(7)}
            >
              <Text style={styles.buttonTextSmall}>7</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.appendInput(8)}
            >
              <Text style={styles.buttonTextSmall}>8</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.appendInput(9)}
            >
              <Text style={styles.buttonTextSmall}>9</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable, styles.bgBlue]}
              onPress={() => this.props.setOperator('multiply')}
            >
              <Text style={styles.buttonTextSmall}>x</Text>
            </TouchableHighlight>
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.appendInput(4)}
            >
              <Text style={styles.buttonTextSmall}>4</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.appendInput(5)}
            >
              <Text style={styles.buttonTextSmall}>5</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.appendInput(6)}
            >
              <Text style={styles.buttonTextSmall}>6</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable, styles.bgBlue]}
              onPress={() => this.props.setOperator('subtract')}
            >
              <Text style={styles.buttonTextSmall}>-</Text>
            </TouchableHighlight>
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.appendInput(1)}
            >
              <Text style={styles.buttonTextSmall}>1</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.appendInput(2)}
            >
              <Text style={styles.buttonTextSmall}>2</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.appendInput(3)}
            >
              <Text style={styles.buttonTextSmall}>3</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable, styles.bgBlue]}
              onPress={() => this.props.setOperator('add')}
            >
              <Text style={styles.buttonTextSmall}>+</Text>
            </TouchableHighlight>
          </View>
        </View>
        <View style={styles.row}>
          <View style={[styles.button, { width: '50%' }]}>
            <TouchableHighlight
              style={[styles.touchable, { width: 175 }]}
              onPress={() => this.props.appendInput(0)}
            >
              <Text style={styles.buttonTextSmall}>0</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.appendInput('.')}
            >
              <Text style={styles.buttonTextSmall}>.</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable, styles.bgBlue]}
              onPress={() => this.props.evaluateInput()}
            >
              <Text style={styles.buttonTextSmall}>=</Text>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    )
  }
}
