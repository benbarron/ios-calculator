import React, { Component } from 'react'

import { View, Text, TouchableHighlight } from 'react-native'

import styles from './ButtonStyleSheet'

export default class ComplexButtons extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View>
        <View style={styles.row}>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable]}
              onPress={() => this.props.complexEvaluation('sin')}
            >
              <Text style={[styles.buttonTextSmall]}>Sin</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable]}
              onPress={() => this.props.complexEvaluation('cos')}
            >
              <Text style={[styles.buttonTextSmall, styles.buttonTextSmall]}>
                Cos
              </Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable]}
              onPress={() => this.props.complexEvaluation('tan')}
            >
              <Text style={[styles.buttonTextDark, styles.buttonTextSmall]}>
                Tan
              </Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable, styles.bgBlue]}
              onPress={() => this.props.toRadians()}
            >
              <Text style={[styles.buttonTextDark, styles.buttonTextSmall]}>
                Rads
              </Text>
            </TouchableHighlight>
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.complexEvaluation('sinh')}
            >
              <Text style={[styles.buttonText, styles.buttonTextSmall]}>
                Sinh
              </Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.complexEvaluation('cosh')}
            >
              <Text style={[styles.buttonText, styles.buttonTextSmall]}>
                Cosh
              </Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.complexEvaluation('tanh')}
            >
              <Text style={[styles.buttonText, styles.buttonTextSmall]}>
                Tanh
              </Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable, styles.bgBlue]}
              onPress={() => this.props.toDegrees()}
            >
              <Text style={[styles.buttonText, styles.buttonTextSmall]}>
                Degs
              </Text>
            </TouchableHighlight>
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.complexEvaluation('exp')}
            >
              <Text style={styles.buttonTextSmall}>e</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.complexEvaluation('naturalLog')}
            >
              <Text style={styles.buttonTextSmall}>ln</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.complexEvaluation('log')}
            >
              <Text style={styles.buttonTextSmall}>log</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable, styles.bgBlue]}
              onPress={() => this.props.appendInput(Math.PI)}
            >
              <Text style={styles.buttonTextSmall}>PI</Text>
            </TouchableHighlight>
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.complexEvaluation('inverse')}
            >
              <Text style={styles.buttonTextSmall}>1/x</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.complexEvaluation('factorial')}
            >
              <Text style={styles.buttonTextSmall}>x!</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.complexEvaluation('sqrt')}
            >
              <Text style={styles.buttonTextSmall}>sqrt</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable, styles.bgBlue]}
              onPress={() => this.props.randomNumber()}
            >
              <Text style={styles.buttonTextSmall}>Rand</Text>
            </TouchableHighlight>
          </View>
        </View>
        <View style={styles.row}>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.complexEvaluation('squared')}
            >
              <Text style={styles.buttonTextSmall}>^2</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.complexEvaluation('cubed')}
            >
              <Text style={styles.buttonTextSmall}>^3</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={styles.touchable}
              onPress={() => this.props.setOperator('power')}
            >
              <Text style={styles.buttonTextSmall}>^x</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.button}>
            <TouchableHighlight
              style={[styles.touchable, styles.bgBlue]}
              onPress={() => this.props.evaluateInput()}
            >
              <Text style={styles.buttonTextSmall}>=</Text>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    )
  }
}
